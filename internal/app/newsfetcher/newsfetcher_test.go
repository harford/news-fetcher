package newsfetcher

import (
	"bytes"
	"encoding/json"
	"errors"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStoryDetails(t *testing.T) {
	tests := []struct {
		story  storyDetails
		result string
		slice  []string
	}{
		{storyDetails{By: "author",
			Score: 10,
			Time:  0,
			Title: "title"},
			"title by author on 1970-01-01 00:00:00 +0000 UTC, score: 10",
			[]string{"title", "author", "10", "0", ""}},

		{storyDetails{By: "author2",
			Score: 99,
			Time:  1522730754,
			Title: "title 2",
			URL:   "http://example.com"},
			"title 2 <http://example.com> by author2 on 2018-04-03 04:45:54 +0000 UTC, score: 99",
			[]string{"title 2", "author2", "99", "1522730754", "http://example.com"}},
	}

	for _, test := range tests {
		result := test.story.String()
		slice := test.story.toSlice()

		assert.Equal(t, test.result, result)
		assert.Equal(t, test.slice, slice)
	}
}

type fakeHTTPReader struct {
	stories map[int]*storyDetails
}

func (f fakeHTTPReader) GetData(url string) ([]byte, error) {
	if url == topStoriesURL {
		topStories := make([]int, 100)

		for id := range topStories {
			topStories[id] = id
		}

		data, _ := json.Marshal(topStories)

		return data, nil
	} else if strings.HasPrefix(url, itemBaseURL) {
		url = url[:len(url)-len(".json")]
		story := strings.Split(url, "/")
		id, _ := strconv.Atoi(story[len(story)-1])

		data, _ := json.Marshal(f.stories[id])
		return data, nil
	}

	return nil, errors.New("bad url")
}

type badHTTPReader struct {
}

func (f badHTTPReader) GetData(url string) ([]byte, error) {
	return nil, errors.New("bad reader")
}

type badJSONHTTPReader struct {
}

func (f badJSONHTTPReader) GetData(url string) ([]byte, error) {
	return []byte("a"), nil
}

func TestStoryFetcher(t *testing.T) {
	f := hackerNewsStoryFetcher{httpReader: fakeHTTPReader{}}
	stories, err := f.fetchTopStories(10)
	assert.Equal(t, len(stories), 10)
	assert.NoError(t, err)

	f = hackerNewsStoryFetcher{httpReader: badHTTPReader{}}
	stories, err = f.fetchTopStories(10)
	assert.Equal(t, len(stories), 0)
	assert.Error(t, err)

	f = hackerNewsStoryFetcher{httpReader: badJSONHTTPReader{}}
	stories, err = f.fetchTopStories(10)
	assert.Equal(t, len(stories), 0)
	assert.Error(t, err)

	r := fakeHTTPReader{stories: map[int]*storyDetails{}}
	s := storyDetails{
		By:    "author",
		Score: 500,
		Time:  123456789,
		Title: "title",
		URL:   "http://example.com",
	}
	r.stories[99] = &s
	f = hackerNewsStoryFetcher{httpReader: r}
	story, err := f.fetchStory(99)
	assert.NoError(t, err)
	assert.Equal(t, story, &s)

	f = hackerNewsStoryFetcher{httpReader: badHTTPReader{}}
	story, err = f.fetchStory(99)
	assert.Error(t, err)

	f = hackerNewsStoryFetcher{httpReader: badJSONHTTPReader{}}
	story, err = f.fetchStory(99)
	assert.Error(t, err)
}

type badTopStoryFetcher struct {
}

func (n badTopStoryFetcher) fetchTopStories(storyCount int) ([]int, error) {
	return nil, errors.New("failed to fetch top stories")
}

func (n badTopStoryFetcher) fetchStory(storyID int) (*storyDetails, error) {
	return nil, nil
}

type badStoryFetcher struct {
}

func (n badStoryFetcher) fetchTopStories(storyCount int) ([]int, error) {
	stories := make([]int, storyCount)
	return stories, nil
}

func (n badStoryFetcher) fetchStory(storyID int) (*storyDetails, error) {
	return nil, errors.New("failed to fetch story")
}

type fakeStoryFetcher struct {
}

func (n fakeStoryFetcher) fetchTopStories(storyCount int) ([]int, error) {
	stories := make([]int, storyCount)
	return stories, nil
}

func (n fakeStoryFetcher) fetchStory(storyID int) (*storyDetails, error) {
	s := storyDetails{
		By:    "author",
		Score: 500,
		Time:  123456789,
		Title: "title",
		URL:   "http://example.com",
	}
	return &s, nil
}

type badWriter struct {
}

func (b badWriter) Write(p []byte) (n int, err error) {
	return 0, errors.New("failed to write")
}

type badCsvWriter struct {
}

func (f badCsvWriter) Flush() {
}

func (f badCsvWriter) Write([]string) error {
	return errors.New("Failed to write data")
}

func (f badCsvWriter) Error() error {
	return nil
}

type badCsvFlush struct {
}

func (f badCsvFlush) Flush() {
}

func (f badCsvFlush) Write([]string) error {
	return nil
}

func (f badCsvFlush) Error() error {
	return errors.New("Failed to flush data")
}

type fakeCsvWriter struct {
	data [][]string
}

func (f *fakeCsvWriter) Error() error {
	return nil
}

func (f *fakeCsvWriter) Flush() {
}

func (f *fakeCsvWriter) Write(s []string) error {
	f.data = append(f.data, s)
	return nil
}

func TestHNFetcher(t *testing.T) {
	// RetrieveStories

	// fail to fetch top stories
	h := HackerNewsFetcher{storyCount: 10,
		storyFetcher: badTopStoryFetcher{},
	}
	err := h.RetrieveStories()
	assert.Error(t, err)
	assert.Equal(t, len(h.stories), 0)

	// fail to fetch story id
	h = HackerNewsFetcher{storyCount: 10,
		storyFetcher: badStoryFetcher{},
	}
	err = h.RetrieveStories()
	assert.Error(t, err)
	assert.Equal(t, len(h.stories), 0)

	// success
	h = HackerNewsFetcher{storyCount: 10,
		storyFetcher: fakeStoryFetcher{},
	}
	err = h.RetrieveStories()
	assert.Equal(t, len(h.stories), 10)
	assert.NoError(t, err)

	// WriteCSV
	h = HackerNewsFetcher{}
	h.stories = make([]*storyDetails, 1)
	h.stories[0] = &storyDetails{
		By:    "author",
		Score: 500,
		Time:  123456789,
		Title: "title",
		URL:   "http://example.com",
	}

	// fail to write file
	err = h.WriteCSV(badCsvWriter{})
	assert.Error(t, err)

	// fail to flush
	err = h.WriteCSV(badCsvFlush{})
	assert.Error(t, err)

	// success - check data
	f := fakeCsvWriter{data: [][]string{}}
	err = h.WriteCSV(&f)
	assert.Equal(t, f.data[0], []string{"title", "author", "500", "123456789", "http://example.com"})
	assert.NoError(t, err)

	// WriteHumanReadable
	h = HackerNewsFetcher{}
	h.stories = make([]*storyDetails, 1)
	h.stories[0] = &storyDetails{
		By:    "author",
		Score: 500,
		Time:  123456789,
		Title: "title",
		URL:   "http://example.com",
	}

	// fail to write string
	err = h.WriteHumanReadable(badWriter{})
	assert.Error(t, err)

	// success - check data
	var b bytes.Buffer
	err = h.WriteHumanReadable(&b)
	assert.Equal(t, "title <http://example.com> by author on 1973-11-29 21:33:09 +0000 UTC, score: 500\n", b.String())
	assert.NoError(t, err)
}
