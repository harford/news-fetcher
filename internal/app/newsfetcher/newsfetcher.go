package newsfetcher

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

/*

Hacker News API publishes the 100 top story IDs:

https://hacker-news.firebaseio.com/v0/topstories.json

[
16707421,
16708259,
16706803,
16706326 ... ]

Each ID can then be queried for details:

https://hacker-news.firebaseio.com/v0/item/16707421.json

{
"by": "thisisit",
"descendants": 179,
"id": 16707421,
"kids": [ 16708725, 16707682 ],
"score": 186,
"time": 1522334450,
"title": "Wall Street rethinks blockchain projects as euphoria meets reality",
"type": "story",
"url": "https://www.reuters.com/article/us-banks-fintech-blockchain/wall-street-rethinks-blockchain-projects-as-euphoria-meets-reality-idUSKBN1H32GO"
}

*/

const (
	itemBaseURL     = "https://hacker-news.firebaseio.com/v0/item/"
	topStoriesURL   = "https://hacker-news.firebaseio.com/v0/topstories.json"
	hnTopStoryCount = 100 // The HN API always returns 100 top stories
)

type storyFetcher interface {
	fetchTopStories(storyCount int) ([]int, error)
	fetchStory(storyID int) (*storyDetails, error)
}

type httpReader interface {
	GetData(url string) ([]byte, error)
}

type csvWriter interface {
	Error() error
	Flush()
	Write([]string) error
}

type liveHTTPReader struct {
}

type hackerNewsStoryFetcher struct {
	httpReader httpReader
}

type storyDetails struct {
	By    string `json:"by"`
	Score int    `json:"score"`
	Time  int64  `json:"time"`
	Title string `json:"title"`
	URL   string `json:"url"`
}

// HackerNewsFetcher retrieves and displays Hacker News top stories
type HackerNewsFetcher struct {
	storyCount   int             // The number of stories to print
	stories      []*storyDetails // The resulting stories fetched from HN
	storyFetcher storyFetcher
}

// GetData returns a slice of bytes from a URL
func (liveHTTPReader) GetData(url string) ([]byte, error) {
	resp, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	return body, nil
}

// toSlice converts story details to a slice of strings appropriate for CSV formatting
func (s *storyDetails) toSlice() []string {
	return []string{s.Title,
		s.By,
		strconv.Itoa(s.Score),
		strconv.FormatInt(s.Time, 10),
		s.URL}
}

// String converts story details to a string for human consumption
func (s *storyDetails) String() string {
	var url string

	// Ask HN, etc don't have a URL
	if s.URL != "" {
		url = fmt.Sprintf("<%s> ", s.URL)
	}

	return fmt.Sprintf("%s %sby %s on %s, score: %d",
		s.Title, url, s.By, time.Unix(s.Time, 0).UTC(), s.Score)
}

// NewHackerNewsFetcher creates a new news fetcher
func NewHackerNewsFetcher(storyCount int) *HackerNewsFetcher {
	n := HackerNewsFetcher{storyCount: storyCount,
		storyFetcher: hackerNewsStoryFetcher{
			httpReader: liveHTTPReader{},
		},
	}
	return &n
}

// RetrieveStories gets a list of top stories and stores it in the HackerNewsFetcher struct
func (n *HackerNewsFetcher) RetrieveStories() error {
	stories, err := n.storyFetcher.fetchTopStories(n.storyCount)
	if err != nil {
		return err
	}

	// This could be a goroutine that fetched in parallel but I
	// don't know what sort of rate limiting the HN API might
	// have, so I think this is safer.
	for _, id := range stories {
		story, err := n.storyFetcher.fetchStory(id)

		if err != nil {
			return err
		}

		n.stories = append(n.stories, story)
	}

	return nil
}

// WriteCSV generates a CSV of top stories
func (n *HackerNewsFetcher) WriteCSV(w csvWriter) error {
	for _, story := range n.stories {
		if err := w.Write(story.toSlice()); err != nil {
			return err
		}
	}

	w.Flush()

	if err := w.Error(); err != nil {
		return err
	}

	return nil
}

// WriteHumanReadable writes the top stories to a stream
func (n *HackerNewsFetcher) WriteHumanReadable(w io.Writer) error {
	for _, story := range n.stories {
		_, err := w.Write([]byte(story.String() + "\n"))
		if err != nil {
			return err
		}
	}

	return nil
}

// FetchTopStories gets an slice of top story IDs
func (n hackerNewsStoryFetcher) fetchTopStories(storyCount int) ([]int, error) {
	stories := make([]int, hnTopStoryCount)

	data, err := n.httpReader.GetData(topStoriesURL)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &stories)

	if err != nil {
		return nil, err
	}

	return stories[:storyCount], nil
}

// FetchStory gets the story details for a specific storyID
func (n hackerNewsStoryFetcher) fetchStory(storyID int) (*storyDetails, error) {
	story := &storyDetails{}

	storyURL := fmt.Sprintf("%s%d.json", itemBaseURL, storyID)
	data, err := n.httpReader.GetData(storyURL)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &story)

	if err != nil {
		return nil, err
	}

	return story, nil
}
