.PHONY: all build test clean

all: build

test:
	go test -cover -race -v gitlab.com/harford/news-fetcher/internal/...

clean:
	rm -f news_fetcher

build:
	go build gitlab.com/harford/news-fetcher/cmd/news_fetcher

lint:
	go fmt gitlab.com/harford/news-fetcher/cmd/... gitlab.com/harford/news-fetcher/internal/...
	go vet gitlab.com/harford/news-fetcher/cmd/... gitlab.com/harford/news-fetcher/internal/...
	golint cmd/... internal/...
