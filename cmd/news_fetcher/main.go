package main

import (
	"encoding/csv"
	"flag"
	"log"
	"os"

	"gitlab.com/harford/news-fetcher/internal/app/newsfetcher"
)

var printCSV = flag.String("csv", "", "File to output in CSV format")

const stories = 20

/*

# Design Notes:

Because this was a simple utility, I didn't put a lot of effort into
reducing memory usage. If it was passing around large amounts of
memory, I would have looked into using pipes to transform the raw
HTTP data -> JSON -> CSV or text.

# Things that weren't specified that I'd ask for a spec:

* how to deal with failures?
  * don't display anything unless all details are read
  * OR display details as they arrive
* format of the human readable mode
* does the CSV need headers?
* format of timestamp for human and CSV

*/

func main() {
	flag.Parse()

	n := newsfetcher.NewHackerNewsFetcher(stories)

	err := n.RetrieveStories()
	if err != nil {
		log.Fatal(err)
	}

	if *printCSV != "" {
		f, err := os.Create(*printCSV)
		if err != nil {
			log.Fatal(err)
		}

		w := csv.NewWriter(f)

		if err = n.WriteCSV(w); err != nil {
			log.Fatal(err)
		}

		return
	}

	if err = n.WriteHumanReadable(os.Stdout); err != nil {
		log.Fatal(err)
	}
}
